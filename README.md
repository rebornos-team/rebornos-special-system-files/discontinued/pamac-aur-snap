# pamac-aur-snap

A Gtk3 frontend for libalpm

https://gitlab.manjaro.org/applications/pamac

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/pamac/pamac-aur-snap.git
```

Files required:

apparmor
<br>
snapd
<br>
snapd-glib
<br>


<br><br><br>

**IMPORTANT NOTE:**


For KDE, **pamac-tray-appindicator** is no longer available.
Instead, you have to uninstall **archlinux-appstream-data** and install **archlinux-appstream-data-pamac**.

